Attribute VB_Name = "Functions"
Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum


Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function PathExists(pPath As String) As Boolean
    On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(ByVal pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function Rellenar_Space(intValue, intDigits, car)
    '*** ESPACIOA A LA IZQ
    mValorLon = intDigits - Len(intValue)
    Rellenar_Space = String(IIf(mValorLon < 0, intDigits, mValorLon), car) & intValue
    'MsgBox String(IIf(mValorLon < 0, intDigits, mValorLon), car) & intValue
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function Rellenar_Blanco(intValue, intDigits)
    '*** ESPACIOA A LA DER
    mValorLon = intDigits - Len(intValue)
    Rellenar_Blanco = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), " ")
End Function


' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For i = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(i = 0, vbNullString, "|") + pRs.Fields(i).Name
    Next i
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.

' Parametros:

' 1) pRs: Recordset
' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).

' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    
    On Error GoTo FuncError
    
    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
    Dim Data As Boolean
    
    If OnlyCurrentRow Then pRowNumAlias = vbNullString
    
    bRowNumAlias = (pRowNumAlias <> vbNullString)
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
    
    If bRowNumAlias Then
        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
        RowNumCount = 0
        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
        mColLength(0) = Len(RowNumAlias)
    End If
    
    For i = 0 To pRs.Fields.Count - 1
        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString))
    Next i
    
    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
    
    If Data And Not OnlyCurrentRow Then
        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
        If pRs.CursorType <> adOpenForwardOnly Then
            pRs.MoveFirst
        Else
            pRs.Requery
        End If
        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
    Else
        Set pRsValues = pRs
    End If
    
    RowNumCount = 0
    
    Do While Not pRs.EOF
        
        'RsPreview = RsPreview & vbNewLine
        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
            'RsPreview = RsPreview & TmpRsValue
        End If
        
        For i = 0 To pRs.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
                        
            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
            End If
            'RsPreview = RsPreview & TmpRsValue
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRs.MoveNext
        
    Loop
    
    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
    
    For i = 0 To pRs.Fields.Count - 1
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString), _
        mColLength(mColArrIndex), " ") & vbTab, , 1)
    Next i
    
    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
        Set pRsValues = pRs
        If Data Then
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    RowNumCount = 0
    
    Do While Not pRsValues.EOF
        
        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
        End If
        
        For i = 0 To pRsValues.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
            Else
                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
            End If
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRsValues.MoveNext
        
    Loop
    
    PrintRecordset = RsPreview
    
Finally:
    
    On Error Resume Next
    
    If Data And Not OnlyCurrentRow Then
        If OriginalRsPosition <> -1 Then
            If pRs.CursorType = adOpenForwardOnly Then
                pRs.Requery
                pRs.Move OriginalRsPosition, 0
            Else
                SafePropAssign pRs, "Bookmark", OriginalRsPosition
            End If
        Else
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    Exit Function
    
FuncError:
    
    'Resume ' Debug
    
    PrintRecordset = vbNullString
    
    Resume Finally
    
End Function

' Imprime el Record Actual de un Recordset.

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

Public Sub SendKeys(Caracteres As String, Optional espera As Boolean = False, Optional Repetir As Boolean = False)
    Dim Teclas As Object
    Set Teclas = CreateObject("DLLKeyboard.DLLTeclado")
    Teclas.SendKeys Caracteres, espera, Repetir
    Set Teclas = Nothing
End Sub

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional pUseUnicode As Boolean = False) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(pFilePath, True, pUseUnicode)
    TStream.Write (pContents)
    TStream.Close
    
    SaveTextFile = True

    Exit Function
    
ErrFile:
    
    Debug.Print Err.Description
    
End Function
