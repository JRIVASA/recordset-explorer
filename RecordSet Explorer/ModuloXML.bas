Attribute VB_Name = "ModuloXML"
' -------------------------------------------------------------------------------

' El metodo se utiliza usando Call ModuloXML.Crear_Xml_Definido (Recordset, RutaArchivo, Arreglo con informacion de los campos)

Public Sub Crear_Xml_Definido(Rs1, ruta As String, ByVal Campos As Variant, _
Optional ByVal MantenerOrdenCampos As Boolean = False)
    
    ' metodo para imprimir XML a partir de un recordset
    ' es necesario que este metodo reciba el RS en el primer registro, y una ruta valida, asi mismo como un arreglo bidimensional con la siguiente estructura:
    ' [ arr (0 to x,0 to 1) ] para saber que campos ense�ar y con que nombre cargarlos en el archivo
    ' los campos que no esten definidos en el archivo no se agregaran en el reporte.
    ' la validacion sobre si el campo es "" es debido a un comportamiento de los RS jerarquicos (shape) para
    ' que no muestre unos campos en blanco que genera el metodo shape
    ' el metodo para obtener la ruta ya depende de como se vaya a implementar la ventana en las pruebas se uso un commonDialog
    ' NOTA: Para mostrar los campos en Recordsets Hijos (Detalles) es necesario llamar al campo en el arreglo de la siguiente manera:
    ' "NombreDelCampo_NombredelRecordsetHijo" Ejemplo: Si el Campo es c_Descri y el Recordset en el Shape lo llaman DetalleProductos
    ' El arreglo se define de la siguiente manera:
    ' Campos(1, 0) = "c_Descri_DetalleProductos"
    ' Campos(1, 1) = "Articulo"
    
    If TypeName(Campos) = "Dictionary" Then
        Crear_Xml_Definido_V2 Rs1, ruta, Campos, MantenerOrdenCampos
        Exit Sub
    End If
    
    Dim ObjDom
    Dim ObjRoot
    Dim ObjField
    Dim ObjFieldValue
    Dim ObjColName
    Dim ObjAttTabOrder
    Dim ObjPI
    Dim X
    Dim ObjRsField
    Dim ObjRow
    Dim ObjInnerRow
    Dim Campo As Variant
    
    If Not Rs1.EOF Then
    
        'Set objDom = Server.CreateObject("Microsoft.XMLDOM")
        Set ObjDom = New MSXML2.DOMDocument60 'DOMDocument
        
        'objDom.preserveWhiteSpace = True
        Set ObjRoot = ObjDom.createElement("ReporteStellar")
        ObjDom.appendChild ObjRoot
     
        Do While Not Rs1.EOF
            
            Set ObjRow = ObjDom.createElement("Registro")
            
            If Not MantenerOrdenCampos Then
                
                ' No mantiene ordenados los campos seg�n el arreglo, solo muestra los campos segun el orden en el que los encuentre...
                
                For Each ObjRsField In Rs1.Fields
                    
                    If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                        
                        'Debug.Print objRSField.Name
                        Call Registro_Interno_Definido(ObjRsField.Value, ObjDom, ObjRow, Campos, ObjRsField.Name)
                        
                    Else
    
                        For i = LBound(Campos) To UBound(Campos)
                            
                            'Debug.Print campos(i, 0)
                            
                            If UCase(Campos(i, 0)) = UCase(ObjRsField.Name) Then
                                
                                Set ObjField = ObjDom.createElement(Campos(i, 1))
                                
                                If Not IsNull(ObjRsField.Value) Then
                                    ObjField.Text = ObjRsField.Value
                                Else
                                    ObjField.Text = "N/A"
                                End If
                                
                                ObjRow.appendChild ObjField
                                
                            End If
                            
                        Next i
                        
                    End If
                    
                Next
                
            Else
                
                ' Este m�todo es solo posible para los reportes que no tienen informaci�n jerarquica, recordsets hijos ni consultas shape.
                ' Sino obviamente dar� error.
                
                For i = LBound(Campos) To UBound(Campos)
                    
                    Set ObjRsField = Rs1.Fields(Campos(i, 0))
                    
                    If UCase(Campos(i, 0)) = UCase(ObjRsField.Name) Then
                        
                        Set ObjField = ObjDom.createElement(Campos(i, 1))
                        
                        If Not IsNull(ObjRsField.Value) Then
                            ObjField.Text = ObjRsField.Value
                        Else
                            ObjField.Text = "N/A"
                        End If
                        
                        ObjRow.appendChild ObjField
                        
                    End If
                    
                Next i
                
            End If
            
            ObjRoot.appendChild ObjRow
            Rs1.MoveNext
            
        Loop
        
        Set ObjPI = ObjDom.createProcessingInstruction("xml", "version='1.0'")
        
        ObjDom.insertBefore ObjPI, ObjDom.childNodes(0)
        
        ObjDom.Save ruta
        
        Set ObjDom = Nothing
        Set ObjRoot = Nothing
        Set ObjField = Nothing
        Set ObjFieldValue = Nothing
        Set ObjColName = Nothing
        Set ObjAttTabOrder = Nothing
        Set ObjPI = Nothing
        
        MsgBox True, "Archivo Generado con �xito."
        
    End If
    
    'Debug.Print objDom.Xml
     
    'rs1.Close
    
End Sub

Public Sub Crear_Xml_Definido_V2(Rs1, ruta As String, ByVal Campos As Variant, _
Optional ByVal MantenerOrdenCampos As Boolean = False)
    
    'On Error GoTo InternalError ' Internal Debug
    
    ' Metodo para imprimir XML a partir de un Recordset
    ' Es necesario que este metodo reciba el RS en el primer registro, y una Ruta v�lida, asi mismo como un Diccionario (Dictionary) con la siguiente estructura:
    ' Campos("N")(0 to 1) para saber que campos ense�ar y con que nombre cargarlos en el archivo.
    ' Los campos que no esten definidos en el archivo no se agregaran en el reporte.
    ' �a validacion sobre si el campo es "" es debido a un comportamiento de los RS jerarquicos (shape) para
    ' que no muestre unos campos en blanco que genera el metodo shape
    ' El metodo para obtener la ruta ya depende de como se vaya a implementar la ventana en las pruebas se uso un commonDialog
    ' NOTA: Para mostrar los campos en Recordsets Hijos (Detalles) es necesario llamar al campo en el Arreglo Interno de la siguiente manera:
    ' "NombreDelCampo_NombredelRecordsetHijo" Ejemplo: Si el Campo es c_Descri y el Recordset en el Shape lo llaman DetalleProductos
    ' El arreglo se define de la siguiente manera:
    ' Campos("N")(0) = "c_Descri_DetalleProductos"
    ' Campos("N")(1) = "Articulo"
    
    ' Se utiliza un diccionario que contiene arrays ya que es una MARAVILLA din�mica
    ' que permite optimizar y agilizar la creaci�n del Esquema Campos.
    ' La sintaxis es tan facil como escribir:
    
    ' Dim X As Dictionary: Set X = New Dictionary
    '
    ' X("1")=array("NombreCampo1", "Etiqueta1")
    ' X(CStr(N)) = array("NombreCampoN", "EtiquetaN")
    '
    ' As� de Simple...
    
    Dim Datos As Variant
    
    Dim ObjDom
    Dim ObjRoot
    Dim ObjField
    Dim ObjFieldValue
    Dim ObjColName
    Dim ObjAttTabOrder
    Dim ObjPI
    Dim X
    Dim ObjRsField
    Dim ObjRow
    Dim ObjInnerRow
    Dim Campo As Variant
    
    If Not Rs1.EOF Then
    
        'Set objDom = Server.CreateObject("Microsoft.XMLDOM")
        Set ObjDom = New MSXML2.DOMDocument60 'DOMDocument
        
        'objDom.preserveWhiteSpace = True
        Set ObjRoot = ObjDom.createElement("ReporteStellar")
        ObjDom.appendChild ObjRoot
     
        Do While Not Rs1.EOF
            
            Set ObjRow = ObjDom.createElement("Registro")
            
            If Not MantenerOrdenCampos Then
                
                ' No mantiene ordenados los campos seg�n el arreglo, solo muestra los campos segun el orden en el que los encuentre...
                
                For Each ObjRsField In Rs1.Fields
                    
                    If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                        
                        'Debug.Print objRSField.Name
                        Call Registro_Interno_Definido_V2(ObjRsField.Value, ObjDom, ObjRow, Campos, ObjRsField.Name)
                        
                    Else
    
                        For Each Datos In AsEnumerable(Campos.Items())
                            
                            'Debug.Print Datos(0)
                            
                            If UCase(Datos(0)) = UCase(ObjRsField.Name) Then
                                
                                Set ObjField = ObjDom.createElement(Datos(1))
                                
                                If Not IsNull(ObjRsField.Value) Then
                                    ObjField.Text = ObjRsField.Value
                                Else
                                    ObjField.Text = "N/A"
                                End If
                                
                                ObjRow.appendChild ObjField
                                
                                Exit For
                                
                            End If
                            
                        Next
                        
                    End If
                    
                Next
                
            Else
                
                ' Este m�todo es solo posible para los reportes que no tienen informaci�n jerarquica,
                ' recordsets hijos ni consultas shape. Sino obviamente dar� error. Cambiado en 2017-09-30
                
                ' ACTUALIZACI�N: Ahora con estas modificaciones si ser� posible mostrar los campos
                ' seg�n el orden especificado hasta para recordsets anidados. Pero para que eso sea
                ' posible, cada campo debe definirse bajo la siguiente sintaxis:
                 
                ' "NombreCampo[_RecordsetLvlN][_RecordsetLvlN-1][_RecordsetLvlN-2][_RecordsetLvlN-m]
                ' Es decir.... primero viene el nombre del campo, y luego los recordsets pero al reves
                ' colocando desde el ultimo nivel hasta el recordset Lvl2.
                 
                ' Ejemplo Rs No Jerarquico:
                
                ' Campos(0) = Array("NombreCampo1", "Etiqueta1")
                ' Campos(1) = Array("NombreCampo2", "Etiqueta2")
                 
                ' Ejemplo Rs Maestro - Detalle (Lvl2)
                
                ' Campos(0) = Array("NombreCampo1", "Etiqueta1") ' Lvl1
                ' Campos(1) = Array("NombreCampo2", "Etiqueta2") ' Lvl1
                ' Campos(2) = Array("NombreCampo1_NombreCampoRsLvl2", "Etiqueta1") ' Lvl2
                ' Campos(3) = Array("NombreCampo2_NombreCampoRsLvl2", "Etiqueta2") ' Lvl2
                
                ' Ejemplo Rs Maestro - Detalle (Lvl3)
                
                ' Campos(0) = Array("NombreCampo1", "Etiqueta1") ' Lvl1
                ' Campos(1) = Array("NombreCampo2", "Etiqueta2") ' Lvl1
                ' Campos(2) = Array("NombreCampo1_NombreCampoRsLvl2", "Etiqueta1") ' Lvl2
                ' Campos(3) = Array("NombreCampo2_NombreCampoRsLvl2", "Etiqueta2") ' Lvl2
                ' Campos(2) = Array("NombreCampo1_NombreCampoRsLvl2_NombreCampoRsLvl3", "Etiqueta1") ' Lvl3
                ' Campos(3) = Array("NombreCampo2_NombreCampoRsLvl2_NombreCampoRsLvl3", "Etiqueta2") ' Lvl3
                
                ' Y As� sucesivamente.
                
                ' Recomendaciones para asegurar el funcionamiento de esta funci�n:
                
                ' Utilizar Nombres de Campos distintos en todos los recordsets
                ' Para que no haya ningun problema (Aunque eso implique cambiar
                ' Nombres de campos en Recordsets y Datareports). Por ejemplo, no hacer esto:
                
                ' En el RsLvl1:  "Lvl1" (Siendo un Campo normal en RsLvl1)
                ' En el RsLvl2:  "Lvl1_Lvl2" (Siendo este el nombre del campo RsLvl3 en RsLvl2)
                ' En el RsLvl3:  "Lvl1_Lvl2_Lvl1" (Siendo este un campo en RsLvl3 a la
                ' vez que el mismo se llamaba Lvl1 dentro del Recordset Lvl2").
                
                ' Es l�gico que mezclar nombres de campos as� sea en niveles distintos de recordsets
                ' Podr�a ser problem�tico a la hora de asegurar el ordenamiento y el funcionamiento
                ' apropiado de este m�todo. Si esta funci�n da error eso ser�a lo primero que revisar.
                
                Dim RetryRsName As Boolean, ChildRs As Variant
                
                For Each Datos In AsEnumerable(Campos.Items())
                    
RetryDatos:
                    
                    On Error GoTo ErrorNotFound
                    
                    RetryRsName = False
                    
                    Set ObjRsField = Rs1.Fields(Datos(0))
                    
                    If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                        
                        'Debug.Print objRSField.Name
                        If Not CBool(ObjRow.getElementsByTagName(StrConv(ObjRsField.Name, vbProperCase)).Length) Then
                            Call Registro_Interno_Definido_V2(ObjRsField.Value, ObjDom, ObjRow, Campos, ObjRsField.Name, MantenerOrdenCampos)
                        End If
                        
                    Else
                        
                        Set ObjField = ObjDom.createElement(Datos(1))
                        
                        If Not IsNull(ObjRsField.Value) Then
                            ObjField.Text = ObjRsField.Value
                        Else
                            ObjField.Text = "N/A"
                        End If
                        
                        ObjRow.appendChild ObjField
                                                
                    End If
                    
ContinueRsName:
                    
                    If RetryRsName Then
                        If Datos(0) Like "*_*" Then
                            ChildRs = InStrRev(Datos(0), "_")
                            If ChildRs > 0 Then ChildRs = Mid(Datos(0), ChildRs + 1)
                            If ChildRs <> vbNullString Then
                                Datos(0) = ChildRs
                                GoTo RetryDatos
                            End If
                        End If
                    End If
                    
                Next
                
ErrorNotFound:
                
                If Err.Number <> 0 Then
                    If Err.Number = 3265 Then ' Field Not Found
                        RetryRsName = True
                        Resume ContinueRsName
                    Else
                        mErrorNumber = Err.Number: mErrorSource = Err.source: mErrorDesc = Err.Description: mErrHF = Err.HelpFile: mErrHC = Err.HelpContext
                        'Resume CheckInternalError
                        
'CheckInternalError:
                        'On Error GoTo InternalError
                        Err.Raise mErrorNumber, mErrorSource, mErrorDesc, mErrHF, mErrHC
                    End If
                End If
                
            End If
            
            ObjRoot.appendChild ObjRow
            Rs1.MoveNext
            
        Loop
        
        Set ObjPI = ObjDom.createProcessingInstruction("xml", "version='1.0'")
        
        ObjDom.insertBefore ObjPI, ObjDom.childNodes(0)
        
        ObjDom.Save ruta
        
        Set ObjDom = Nothing
        Set ObjRoot = Nothing
        Set ObjField = Nothing
        Set ObjFieldValue = Nothing
        Set ObjColName = Nothing
        Set ObjAttTabOrder = Nothing
        Set ObjPI = Nothing
        
        MsgBox True, "Archivo Generado con �xito."
        
    End If
    
    'Debug.Print objDom.Xml
     
    'rs1.Close
    
    Exit Sub
    
'InternalError:
    
    'Resume ' Debug
    
    'mErrorNumber = Err.Number
    'mErrorDesc = Err.Description
    'mErrorSource = Err.Source
    
    'MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

'este metodo fue una version del anterior que es el definitivo, este imprime todos los campos del recordset, tras una modificacion no he determinado por que falla,
'luis barrios 15/05/2014

Private Sub Crear_Xml(Rs1, ruta As String)
    
    Dim Xml As MSXML2.DOMDocument60 'DOMDocument
    Dim ObjDom
    Dim ObjRoot
    Dim ObjField
    Dim ObjFieldValue
    Dim ObjColName
    Dim ObjAttTabOrder
    Dim ObjPI
    Dim X
    Dim ObjRsField
    Dim ObjRow
    Dim ObjInnerRow

    If Not Rs1.EOF Then
    
        'Set objDom = Server.CreateObject("Microsoft.XMLDOM")
        Set ObjDom = New MSXML2.DOMDocument60 'DOMDocument
        ObjDom.preserveWhiteSpace = True
        Set ObjRoot = ObjDom.createElement("ReporteStellar")
        ObjDom.appendChild ObjRoot
        
        Do While Not Rs1.EOF
            Set ObjRow = ObjDom.createElement("Registro")
        
            For Each ObjRsField In Rs1.Fields
        
                If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                    Call Registro_Interno(ObjRsField.Value, ObjDom, ObjRow)
                Else
                    If Not IsNull(ObjRsField.Value) Then
                        If ObjRsField.Value <> "" And ObjRsField.Value <> " " Then
                            Set ObjField = ObjDom.createElement(ObjRsField.Name)
                            
                            Debug.Print ObjRsField.Name
                            Debug.Print ObjRsField.Value
                            ObjField.Text = ObjRsField.Value
                            ObjRow.appendChild ObjField
                        End If
                    End If
                End If
        
            Next
        
            ObjRoot.appendChild ObjRow
            Rs1.MoveNext
        Loop
        
        Set ObjPI = ObjDom.createProcessingInstruction("xml", "version='1.0'")
        
        ObjDom.insertBefore ObjPI, ObjDom.childNodes(0)
    
    End If

    ObjDom.Save ruta
    'Debug.Print objDom.Xml
    
    'rs1.Close
    Set ObjDom = Nothing
    Set ObjRoot = Nothing
    Set ObjField = Nothing
    Set ObjFieldValue = Nothing
    Set ObjColName = Nothing
    Set ObjAttTabOrder = Nothing
    Set ObjPI = Nothing
    
    MsgBox "Archivo Generado con �xito."

End Sub

Private Function Registro_Interno_Definido(ByRef Rs, ByRef ObjDom, ByRef ObjRow, Campos As Variant, tabla) As Object
    
    Dim ObjField
    Dim ObjRsField
    Dim ObjInnerRow
    
    Do While Not Rs.EOF
        
        Set ObjInnerRow = ObjDom.createElement("Detalle")
        
        For Each ObjRsField In Rs.Fields
            
            If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                Call Registro_Interno_Definido(ObjRsField.Value, ObjDom, ObjInnerRow, Campos, ObjRsField.Name)
            Else
                
                For i = LBound(Campos) To UBound(Campos)
                    
                    If UCase(Campos(i, 0)) = UCase(ObjRsField.Name & "_" & tabla) Then
                        
                        Set ObjField = ObjDom.createElement(Campos(i, 1))
                        
                        If Not IsNull(ObjRsField.Value) Then
                            ObjField.Text = ObjRsField.Value
                        Else
                            ObjField.Text = "N/A"
                        End If
                        
                        ObjInnerRow.appendChild ObjField
                        
                    End If
                    
                Next i
                
            End If
            
        Next
        
        ObjRow.appendChild ObjInnerRow
        
        Rs.MoveNext
        
    Loop
    
    Set ObjField = Nothing
    Set ObjRsField = Nothing
    Set ObjInnerRow = Nothing
    
End Function

Private Function Registro_Interno_Definido_V2(ByRef Rs, ByRef ObjDom, ByRef ObjRow, _
Campos As Variant, tabla, _
Optional ByVal MantenerOrdenCampos As Boolean = False) As Object
    
    Dim Datos As Variant
    
    Dim ObjField
    Dim ObjRsField
    Dim ObjInnerRow
    
    Do While Not Rs.EOF
        
        Set ObjInnerRow = ObjDom.createElement(StrConv(tabla, vbProperCase))
        
        If Not MantenerOrdenCampos Then
            
            For Each ObjRsField In Rs.Fields
                
                If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                    Call Registro_Interno_Definido_V2(ObjRsField.Value, ObjDom, ObjInnerRow, Campos, ObjRsField.Name)
                Else
                    
                    For Each Datos In AsEnumerable(Campos.Items())
                        
                        If UCase(Datos(0)) = UCase(ObjRsField.Name & "_" & tabla) Then
                            
                            Set ObjField = ObjDom.createElement(Datos(1))
                            
                            If Not IsNull(ObjRsField.Value) Then
                                ObjField.Text = ObjRsField.Value
                            Else
                                ObjField.Text = "N/A"
                            End If
                            
                            ObjInnerRow.appendChild ObjField
                            
                            Exit For
                            
                        End If
                        
                    Next
                    
                End If
                
            Next
            
        Else
            
            Dim RetryRsName As Boolean, ChildRs As Variant
            
            For Each Datos In AsEnumerable(Campos.Items())
                
                DatosCeroOriginal = Datos(0)
                
                If Not UCase(Datos(0)) Like UCase("*_" & tabla & "*") Then
                    GoTo Continue
                End If
                
                Dim LastTableName
                
                TmpPos = InStrRev(UCase(Datos(0)), UCase("_" & tabla))
                
                If TmpPos > 0 Then
                    Datos(0) = Mid(Datos(0), 1, TmpPos - 1)
                End If
                
RetryDatos:
                    
                On Error GoTo ErrorNotFound
                    
                RetryRsName = False
                
                TmpPos = InStrRev(UCase(Datos(0)), UCase("_" & LastTableName))
                
                If TmpPos > 0 And Not IsEmpty(LastTableName) Then
                    'TmpPos1 = InStrRev(UCase(DatosOriginal), UCase("_" & tabla))
                    NextTableName = Mid(Datos(0), TmpPos + 1)
                    If NextTableName = LastTableName Then
                        GoTo Continue
                    End If
                End If
                
                Set ObjRsField = Rs.Fields(Datos(0))
                
                If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                    
                    'Debug.Print objRSField.Name
                    LastTableName = ObjRsField.Name
                    'Set ObjField = ObjDom.createElement(Datos(1))
                    Call Registro_Interno_Definido_V2(ObjRsField.Value, ObjDom, ObjInnerRow, Campos, ObjRsField.Name, True)
                    
                Else
                    
                    Set ObjField = ObjDom.createElement(Datos(1))
                    
                    If Not IsNull(ObjRsField.Value) Then
                        ObjField.Text = ObjRsField.Value
                    Else
                        ObjField.Text = "N/A"
                    End If
                    
                    ObjInnerRow.appendChild ObjField
                    
                End If
                
ContinueRsName:
                
                If RetryRsName Then
                    If Datos(0) Like "*_*" Then
                        ChildRs = InStrRev(Datos(0), "_") ' Replace(DatosCeroOriginal, "_" & tabla, vbNullString, 1, 1)
                        If ChildRs > 0 Then ChildRs = Mid(Datos(0), ChildRs + 1)
                        If ChildRs <> vbNullString Then
                            Datos(0) = ChildRs
                            'If Datos(0) = tabla Then
                                GoTo RetryDatos
                            'End If
                        End If
                    End If
                End If
                
Continue:
                
            Next
            
ErrorNotFound:
                
                If Err.Number <> 0 Then
                    If Err.Number = 3265 Then ' Field Not Found
                        RetryRsName = True
                        Resume ContinueRsName
                    Else
                        mErrorNumber = Err.Number: mErrorSource = Err.source: mErrorDesc = Err.Description: mErrHF = Err.HelpFile: mErrHC = Err.HelpContext
                        'Resume CheckInternalError
                        
'CheckInternalError:
                        'On Error GoTo InternalError
                        Err.Raise mErrorNumber, mErrorSource, mErrorDesc, mErrHF, mErrHC
                    End If
                End If
            
        End If
        
        If Len(ObjInnerRow.Text) > 0 Then
            ObjRow.appendChild ObjInnerRow
        End If
        
        LastTableName = Empty
        
        Rs.MoveNext
        
    Loop
    
    Set ObjField = Nothing
    Set ObjRsField = Nothing
    Set ObjInnerRow = Nothing
    
End Function

Private Function Registro_Interno(ByRef Rs, ByRef ObjDom, ByRef ObjRow)
 
    Dim ObjField
    Dim ObjRsField
    Dim ObjInnerRow
     
    Do While Not Rs.EOF
    
        Set ObjInnerRow = ObjDom.createElement("Detalle")
        
        For Each ObjRsField In Rs.Fields
        
            If TypeOf ObjRsField.Value Is ADODB.Recordset Then
                Call Registro_Interno(ObjRsField.Value, ObjDom, ObjInnerRow)
            Else
                Set ObjField = ObjDom.createElement(ObjRsField.Name)
                ObjField.Text = ObjRsField.Value
                ObjInnerRow.appendChild ObjField
            End If
        
        Next
        
        ObjRow.appendChild ObjInnerRow
            
        Rs.MoveNext
        
    Loop

    Set ObjField = Nothing
    Set ObjRsField = Nothing
    Set ObjInnerRow = Nothing

End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

