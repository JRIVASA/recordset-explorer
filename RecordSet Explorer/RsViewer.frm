VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form RsViewer 
   Caption         =   "Recordset Explorer"
   ClientHeight    =   5100
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12330
   LinkTopic       =   "Form1"
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   5100
   ScaleWidth      =   12330
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CommandButton CmdADTGToXML 
      Caption         =   "ADTG>XML"
      Height          =   735
      Left            =   9360
      TabIndex        =   25
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton CmdXMLToADTG 
      Caption         =   "XML>ADTG"
      Height          =   735
      Left            =   8040
      TabIndex        =   24
      Top             =   1200
      Width           =   1095
   End
   Begin VB.CommandButton CmdClean1 
      Height          =   495
      Left            =   12000
      TabIndex        =   22
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox txtCamposDefinidos 
      Height          =   1215
      Left            =   11880
      TabIndex        =   21
      Top             =   840
      Width           =   5055
   End
   Begin VB.CommandButton CmdToNotepad 
      Caption         =   "To Notepad"
      Height          =   735
      Left            =   6840
      TabIndex        =   20
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CommandButton CmdDown 
      Caption         =   "v"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10800
      TabIndex        =   19
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton CmdUp 
      Caption         =   "^"
      Height          =   495
      Left            =   10080
      TabIndex        =   18
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton CmdRight 
      Caption         =   "->"
      Height          =   495
      Left            =   9240
      TabIndex        =   17
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton CmdLeft 
      Caption         =   "<-"
      Height          =   495
      Left            =   8520
      TabIndex        =   16
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton CmdOpenXMLExcel 
      Height          =   495
      Left            =   14400
      OLEDropMode     =   1  'Manual
      Picture         =   "RsViewer.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton CmdOpenXMLDefault 
      Height          =   495
      Left            =   13560
      OLEDropMode     =   1  'Manual
      Picture         =   "RsViewer.frx":3738
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton CmdCopy 
      Caption         =   "Copy To Clip"
      Height          =   735
      Left            =   5280
      TabIndex        =   11
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CommandButton CmdToXml 
      Caption         =   "ExportXML"
      Height          =   735
      Left            =   11880
      TabIndex        =   10
      Top             =   2280
      Width           =   1455
   End
   Begin VB.CheckBox chkEncloseHeaders 
      Caption         =   "Enclose Headers"
      Height          =   375
      Left            =   1920
      TabIndex        =   9
      Top             =   2400
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.CheckBox chkRowNum 
      Caption         =   "Ver Row #"
      Height          =   375
      Left            =   600
      TabIndex        =   8
      Top             =   2400
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.CommandButton CmdPreview 
      Caption         =   "Preview"
      Height          =   735
      Left            =   6360
      TabIndex        =   7
      Top             =   1200
      Width           =   1455
   End
   Begin RichTextLib.RichTextBox rtfView 
      Height          =   4575
      Left            =   360
      TabIndex        =   6
      Top             =   3240
      Width           =   15615
      _ExtentX        =   27543
      _ExtentY        =   8070
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      Appearance      =   0
      OLEDragMode     =   0
      TextRTF         =   $"RsViewer.frx":6E70
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Lucida Console"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton CmdCerrar 
      Caption         =   "Cerrar"
      Height          =   735
      Left            =   3480
      TabIndex        =   5
      Top             =   1200
      Width           =   1455
   End
   Begin VB.CheckBox ChkEstadoRs 
      Caption         =   "Cerrado"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3360
      TabIndex        =   4
      Top             =   2640
      Width           =   1215
   End
   Begin VB.CommandButton CmdAbrir 
      Caption         =   "Abrir"
      Height          =   735
      Left            =   1800
      TabIndex        =   3
      Top             =   1200
      Width           =   1455
   End
   Begin MSComDlg.CommonDialog cdgBuscador 
      Left            =   960
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdBuscarRuta 
      Height          =   495
      Left            =   11160
      TabIndex        =   2
      Top             =   480
      Width           =   615
   End
   Begin VB.TextBox TxtRutaRs 
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      TabIndex        =   0
      Top             =   480
      Width           =   9255
   End
   Begin VB.Label lblRowCount 
      Caption         =   "0 Rows"
      Height          =   255
      Left            =   3360
      TabIndex        =   26
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   $"RsViewer.frx":6EF3
      Height          =   375
      Left            =   13080
      TabIndex        =   23
      Top             =   240
      Width           =   5535
   End
   Begin VB.Label lblOpen2 
      Caption         =   "Excel"
      Height          =   255
      Left            =   14400
      TabIndex        =   15
      Top             =   2160
      Width           =   615
   End
   Begin VB.Label lblOpen1 
      Caption         =   "Default"
      Height          =   255
      Left            =   13560
      TabIndex        =   14
      Top             =   2160
      Width           =   615
   End
   Begin VB.Label lblRuta 
      Caption         =   "Ruta Archivo:"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   1335
   End
End
Attribute VB_Name = "RsViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CmdRawArgs As String
Private CurrentRs As Object
Private CurrentPath As String

Private FormaCargada As Boolean
Private SavedFilePath As String

Private Sub chkEncloseHeaders_Click()
    CmdPreview_Click
End Sub

Private Sub chkRowNum_Click()
    CmdPreview_Click
End Sub

Private Sub CmdAbrir_Click()
    
    CurrentPath = Replace(TxtRutaRs.Text, "$(AppPath)", App.Path)
    
    If Not PathExists(CurrentPath) Then
        CurrentPath = FindPath(CurrentPath, NO_SEARCH_MUST_FIND_EXACT_MATCH, vbNullString)
    End If
    
    If Len(CurrentPath) > 0 Then
        
        On Error GoTo Error
        
        Set CurrentRs = New Recordset
        CurrentRs.Open CurrentPath, "Provider=MSPersist;"
        
        ChkEstadoRs.Value = vbChecked
        ChkEstadoRs.Caption = "Abierto"
        lblRowCount.Caption = FormatNumber(CurrentRs.RecordCount, 0, -1, 0, -1) & " Rows"
        
    End If
    
    Exit Sub
    
Error:

    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.source
    
    MsgBox mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    rtfView.Text = mErrorNumber & GetLines & mErrorDesc & GetLines & mErrorSource
    
    If (mErrorNumber = -2147467259) Then
        
        Dim CurrentErr As ADODB.Error
        
        If Not CurrentRs Is Nothing Then
            
            For Each CurrentErr In CurrentRs.ActiveConnection.Errors
                rtfView.Text = rtfView.Text & GetLines(2) & Rellenar_Space(vbNullString, 50, "-")
                rtfView.Text = rtfView.Text & GetLines & CurrentErr.Number
                rtfView.Text = rtfView.Text & GetLines & CurrentErr.Description
                rtfView.Text = rtfView.Text & GetLines & CurrentErr.source
                rtfView.Text = rtfView.Text & GetLines & CurrentErr.NativeError
                rtfView.Text = rtfView.Text & GetLines & CurrentErr.SQLState
                rtfView.Text = rtfView.Text & GetLines & Rellenar_Space(vbNullString, 50, "-")
            Next
            
            If CurrentRs.ActiveConnection.Errors.Count > 0 Then
                rtfView.Text = rtfView.Text & GetLines(2) & Rellenar_Space(vbNullString, 50, "-")
            End If
            
        End If
        
    End If
    
    CmdCerrar_Click
    
End Sub

Private Sub CmdBuscarRuta_Click()
    
    cdgBuscador.CancelError = False
    
    cdgBuscador.Filter = "(ADTG) (.adtg)|*.adtg|XML (.xml)|*.xml|Todos|*.*"
    
    If Len(TxtRutaRs.Text) > 0 Then
        cdgBuscador.InitDir = cdgBuscador.FileName
    Else
    
    End If
    
    cdgBuscador.ShowOpen
    
    TxtRutaRs.Text = cdgBuscador.FileName
    
End Sub

Private Sub CmdCerrar_Click()
    Set CurrentRs = Nothing
    ChkEstadoRs.Value = vbUnchecked
    ChkEstadoRs.Caption = "Cerrado"
    lblRowCount.Caption = FormatNumber(0, 0, -1, 0, -1) & " Rows"
End Sub

Private Sub CmdClean1_Click()
    txtCamposDefinidos.Text = ""
End Sub

Private Sub CmdCopy_Click()
    CtrlC rtfView.Text
    MsgBox "Copiado"
End Sub

Private Sub CmdDown_Click()
    rtfView_KeyDown vbKeyDown, vbCtrlMask
End Sub

Private Sub CmdLeft_Click()
    'Me.WindowState = 0
    'Me.Width = Me.Width - (0.1 * rtfView.Width)
    rtfView_KeyDown vbKeyLeft, vbCtrlMask
End Sub

Private Sub CmdOpenXMLDefault_Click()
    If Len(SavedFilePath) > 0 Then
        On Error Resume Next
        ShellEx Me.hWnd, "Open", """" & SavedFilePath & """", vbNullString, vbNullString, 1
    End If
End Sub

Private Sub CmdOpenXMLDefault_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Data.Files.Count = 1 Then
        On Error Resume Next
        'ShellEx Me.hWnd, "Open", """" & Data.Files(1) & """", vbNullString, vbNullString, 1
        TxtRutaRs.Text = Data.Files(1)
        CmdAbrir_Click
        CmdPreview_Click
    End If
End Sub

Private Sub CmdOpenXMLExcel_Click()
    If Len(SavedFilePath) > 0 Then
        On Error Resume Next
        ShellEx Me.hWnd, "Open", "EXCEL", """" & SavedFilePath & """", vbNullString, 1
    End If
End Sub

Private Sub CmdOpenXMLExcel_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Data.Files.Count = 1 Then
        On Error Resume Next
        ShellEx Me.hWnd, "Open", "EXCEL", """" & Data.Files(1) & """", vbNullString, 1
    End If
End Sub

Private Sub CmdOpenXMLExcel_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
Debug.Print 1
End Sub

Private Sub CmdOpenXMLExcel_OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean)
Debug.Print 1
End Sub

Private Sub CmdOpenXMLExcel_OLESetData(Data As DataObject, DataFormat As Integer)
Debug.Print 1
End Sub

Private Sub CmdOpenXMLExcel_OLEStartDrag(Data As DataObject, AllowedEffects As Long)
Debug.Print 1
End Sub

Private Sub CmdPreview_Click()
    rtfView.Text = PrintRecordset(CurrentRs, IIf(chkRowNum.Value, "RowNum", ""), , chkEncloseHeaders.Value)
End Sub

Private Sub CmdRight_Click()
    'Me.WindowState = 0
    'Me.Width = Me.Width + (0.1 * rtfView.Width)
    rtfView_KeyDown vbKeyRight, vbCtrlMask
End Sub

Private Sub CmdToNotepad_Click()
    Dim TmpRuta As String
    TmpRuta = App.Path & "\TmpNotepadText.txt"
    rtfView.SaveFile TmpRuta, rtfText
    ShellEx Me.hWnd, "Open", "Notepad.exe", TmpRuta, vbNullString, 1
End Sub

Private Sub CmdToXml_Click()
    
    On Error GoTo Error
    
    OrigFileName = cdgBuscador.FileName
        
    cdgBuscador.Filter = "XML (.xml)|*.xml"
    
    cdgBuscador.ShowSave
    
    CreateFullDirectoryPath (cdgBuscador.FileName)
    
    If PathExists(GetDirParent(cdgBuscador.FileName)) Then
        
        Dim Campos As Dictionary: Set Campos = New Dictionary
        Dim TmpCount As Long: TmpCount = -1
        
        If Len(txtCamposDefinidos.Text) = 0 Then
            
            For Each Itm In CurrentRs.Fields
                TmpCount = TmpCount + 1
                Campos(TmpCount) = Array(Itm.Name, Replace(Itm.Name, " ", "_"))
            Next
            
        Else
            
            For Each Itm In AsEnumerable(Split(txtCamposDefinidos.Text, "|"))
                TmpCount = TmpCount + 1
                Itm2 = Split(Itm, ":")
                If UBound(Itm2) = 0 Then
                    Campos(TmpCount) = Array(Itm2(0), Replace(Itm2(0), " ", "_"))
                Else
                    Campos(TmpCount) = Array(Itm2(0), Itm2(1))
                End If
            Next
            
        End If
        
        If Not (CurrentRs.BOF And CurrentRs.EOF) Then CurrentRs.MoveFirst
        
        Call Crear_Xml_Definido(CurrentRs, cdgBuscador.FileName, Campos, True)
        
        SavedFilePath = cdgBuscador.FileName
        
        lblOpen1.Visible = True
        lblOpen2.Visible = True
        CmdOpenXMLDefault.Visible = True
        CmdOpenXMLExcel.Visible = True
        
    Else
        MsgBox "Ruta invalida."
    End If
    
    cdgBuscador.FileName = OrigFileName
    
    Exit Sub
    
Error:
    
    MsgBox Err.Description & " " & "(" & Err.Number & ")"
    
End Sub

Private Sub CmdUp_Click()
    rtfView_KeyDown vbKeyUp, vbCtrlMask
End Sub

Private Sub CmdXMLToADTG_Click()
    If ChkEstadoRs.Value And UCase(Right(TxtRutaRs.Text, 4)) = ".XML" Then
        On Error Resume Next
        If Not (CurrentRs.BOF And CurrentRs.EOF) Then CurrentRs.MoveFirst
        CurrentRs.Save Mid(TxtRutaRs.Text, 1, Len(TxtRutaRs.Text) - 4) & ".ADTG", adPersistADTG
        If Err Then
            MsgBox Err.Description & " " & "(" & Err.Number & ")"
        Else
            MsgBox "Archivo guardado como ADTG en la misma ruta y mismo nombre."
        End If
    End If
End Sub

Private Sub CmdADTGToXML_Click()
    If ChkEstadoRs.Value And UCase(Right(TxtRutaRs.Text, 5)) = ".ADTG" Then
        On Error Resume Next
        If Not (CurrentRs.BOF And CurrentRs.EOF) Then CurrentRs.MoveFirst
        CurrentRs.Save Mid(TxtRutaRs.Text, 1, Len(TxtRutaRs.Text) - 5) & ".XML", adPersistXML
        If Err Then
            MsgBox Err.Description & " " & "(" & Err.Number & ")"
        Else
            MsgBox "Archivo guardado como XML en la misma ruta y mismo nombre."
        End If
    End If
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        cdgBuscador.InitDir = App.Path
        If Len(CmdRawArgs) > 0 Then
            Dim ruta As String
            ruta = Replace(CmdRawArgs, """", "")
            If PathExists(ruta) Then
                TxtRutaRs.Text = ruta
                CmdAbrir_Click
                If Not CurrentRs Is Nothing Then
                    CmdPreview_Click
                End If
            Else
                MsgBox CmdRawArgs
            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    CmdRawArgs = Command
End Sub

Private Sub Form_OLECompleteDrag(Effect As Long)
Debug.Print 1
End Sub

Private Sub Form_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    CmdOpenXMLDefault_OLEDragDrop Data, Effect, Button, Shift, X, Y
End Sub

Private Sub Form_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
Debug.Print 1
End Sub

Private Sub Form_OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean)
Debug.Print 1
End Sub

Private Sub Form_OLESetData(Data As DataObject, DataFormat As Integer)
Debug.Print 1
End Sub

Private Sub Form_OLEStartDrag(Data As DataObject, AllowedEffects As Long)
Debug.Print 1
End Sub

Private Sub Form_Resize()
    rtfView.Width = Me.Width - rtfView.Left * 3
End Sub

Private Sub rtfView_DragDrop(source As Control, X As Single, Y As Single)
    Debug.Print
End Sub

Private Sub rtfView_KeyDown(KeyCode As Integer, Shift As Integer)
    If Shift = vbCtrlMask Then
        If KeyCode = vbKeyLeft Then
            rtfView.Width = rtfView.Width - (0.1 * rtfView.Width)
        ElseIf KeyCode = vbKeyRight Then
            rtfView.Width = rtfView.Width + (0.1 * rtfView.Width)
        ElseIf KeyCode = vbKeyUp Then
            rtfView.Font.Size = rtfView.Font.Size + 1
            rtfView.SelFontSize = rtfView.SelFontSize + 1
        ElseIf KeyCode = vbKeyDown Then
            rtfView.Font.Size = rtfView.Font.Size - 1
            rtfView.SelFontSize = rtfView.SelFontSize + 1
        End If
    End If
End Sub

Private Sub rtfView_OLECompleteDrag(Effect As Long)
    Debug.Print "ok"
End Sub

Private Sub rtfView_OLEDragDrop(Data As RichTextLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Debug.Print "ok"
End Sub

Private Sub rtfView_OLEDragOver(Data As RichTextLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    Debug.Print "ok"
End Sub

Private Sub rtfView_OLEGiveFeedback(Effect As Long, DefaultCursors As Boolean)
    Debug.Print "ok"
End Sub

Private Sub rtfView_OLESetData(Data As RichTextLib.DataObject, DataFormat As Integer)
    Debug.Print "ok"
End Sub

Private Sub TxtRutaRs_Change()
    CmdCerrar_Click
End Sub

Private Sub TxtRutaRs_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
Debug.Print "ok"
End Sub

Private Sub TxtRutaRs_OLESetData(Data As DataObject, DataFormat As Integer)
Debug.Print "ok"
End Sub
